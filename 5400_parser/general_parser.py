import re
import rx
import json
from rx import operators as ops


def getWLines(lines):
    rxsource = rx.from_(lines)

    def marker(l):
        def extractLine(text):
            regex = '(^\[\d+\])=(.*$)'
            return re.match(regex, text)    

        rgx = extractLine(l)
        if rgx:
            MARKER = ''        
            idx, line = (rgx.groups())

            s = re.search('^\d[\d]*$', line)
            if s:
                MARKER = 'P'        

            s = re.search('^\d+ +.*', line)
            if s:
                MARKER = 'I'

            s = re.search('^.*\([\dA-Z]{5}\)$', line)
            if s:
                MARKER = 'T'        

            s = re.search('^\d\s+[\w\(]+.*\([\dA-Z]{5}\)$', line)
            if s:
                MARKER = 'C'    

            s = re.search('^ECHELON.*CHAIN OF COMMAND.*\([\dA-Z]{5}\)$', line)            
            if s:
                MARKER = 'S'    

            s = re.search('^\([\dA-Z]{5}\)$', line)            
            if s:
                MARKER = 'H'

            s = re.search('^^\d[\d]* [A-Za-z]{3} [\d]{4}$', line)     
            if s:
                MARKER = 'R'

            return '%s<%s>%s' % (idx, MARKER, line)  
        
    case1 = rxsource.pipe(
       ops.map(lambda a: a.strip() )
    )

    skipped_lines=[]

    case1.subscribe(
       on_next = lambda i: skipped_lines.append(i),
       on_error = lambda e: print("Error : {0}".format(e)),
       on_completed = lambda: print('Finished filter job.'),
    )

#     print('starting')
#     print(len(skipped_lines))

    def breakInspect(line):
        regex = '(^\[\d+\])(<\w*>)(.*$)'
        rgx = re.match(regex, line)
        if rgx:
            line = (rgx.groups())
        return line

    rxsource2 = rx.from_(enumerate(skipped_lines)).pipe(
        ops.map(lambda x: '[{0}]={1}'.format(x[0], x[1])),
        ops.map(lambda x: marker(x)),
        ops.map(lambda x: breakInspect(x))
    )

    wlines = []

    rxsource2.subscribe(
       on_next = lambda i: wlines.append(i),
       on_error = lambda e: print("Error : {0}".format(e)),
       on_completed = lambda: print('Processed WLines: %s' % len(wlines))
    ) 
    
    return wlines


def getLineNum(line_num):
    result = 0
    rgx = re.match('^\[(\d+)\]', line_num.strip())
    if rgx:
        result = int(rgx.groups()[0])
    return result     

def checkIfLineIsOpenEnded(line):
    result = False
    rgx1 = re.search('^\(\w+.*$', line)
    rgx2 = re.search('.*\)$', line)
    if rgx1 and rgx2 is None:
        result = True
    else:
        result = False
    return result

def checkIfLineIsClosed(line):
    result = False
    rgx1 = re.search('^\(\w+.*\)$', line)
    if rgx1:
        result = True
    else:
        result = False
    return result


from rx import create
from rx.subject import Subject

def getTransactionLog(wlines):
    transaction_log = []
    observable_lines = wlines[:]

    def observable(observer, scheduler):
        for line_num, tag, line in observable_lines:
            observer.on_next((line_num, tag, line, transaction_log))

        observer.on_completed()

    source = create(observable)

    def markRemoveTag(data):
        line_num, tag, line = data
        transaction_log.append((("%s <R> %s %s" % (line_num, tag, line))))    
    #     print("remove mark")


    def mergeTags(data):
        pline_num, ptag, pline, line_num, tag, line, transaction_log = data
        transaction_log.append((("%s <M> %s %s" % (pline_num, pline, line))))    

    def updateEchelon(data):
        line_num, val, line = data
        newval = ("%s <M> %s %s" % (line_num, val, line))
        transaction_log.append(newval)    

    def actionWord(line_num, span, wlines):
        tag_sequence = []
        num = getLineNum(line_num)

        elements = []

        for i in range(span):
            lno, tag, line = wlines[num - i]
            elements.append((lno, tag, line))
            tag_sequence.append(tag)

        action = "".join(tag_sequence)
        return action, elements


    subjRemove = Subject()
    subjRemove.subscribe(
        lambda x: markRemoveTag(x)
    )    

    subjMerge = Subject()
    subjMerge.subscribe(
        lambda x: mergeTags(x)
    )

    subjUpdateEchelon = Subject()
    subjUpdateEchelon.subscribe(
        lambda x: updateEchelon(x)
    )    
    
    
    
    def processTTag(data):
        def processTI(data):
            line_num, tag, line, transaction_log = data        
            action, elements = actionWord(line_num, 2, wlines)

            if action == "<T><I>":
    #             print("OK", line_num)
                iline_num, itag, iline = elements[1]

                subjMerge.on_next((iline_num, itag, iline, line_num, tag, line, transaction_log))
                subjRemove.on_next((line_num, tag, line)) 

        # [64][71]<>(COMNAVSO DET INTER-AMERICAN NAVAL TELECOMMUNICATIONS
        # [65][72]<T>NETWORK) (32112)            
        def processTE(data):
            line_num, tag, line, transaction_log = data        
            action, elements = actionWord(line_num, 2, wlines)

    #         print(action)

            if action == "<T><>":
    #             print("OK", line_num, tag, line)
                eline_num, etag, eline = elements[1]

                subjMerge.on_next((eline_num, etag, eline, line_num, tag, line, transaction_log))
                subjRemove.on_next((line_num, tag, line)) 


        # [10][13]<C>3 COMMANDER, U.S. SIXTH FLEET (3818A)
        # [11][14]<T>(COMSIXTHFLT MCM DET ROTA) (40366) 
        def processTC(data):
            line_num, tag, line, transaction_log = data        
            action, elements = actionWord(line_num, 2, wlines)


            if action == "<T><C>":

                cline_num, ctag, cline = elements[1]

                rgx = re.match('^(\d[\d]*).*$', cline)
                if rgx:
                    val = rgx.groups()[0]
                    subjUpdateEchelon.on_next((line_num, val, line))


        subjTI = Subject()
        subjTI.subscribe(
            lambda x: processTI(x)        
        ) 


        subjTE = Subject()
        subjTE.subscribe(
            lambda x: processTE(x)        
        ) 


        subjTC = Subject()
        subjTC.subscribe(
            lambda x: processTC(x)        
        ) 


        subjTI.on_next(data)
        subjTE.on_next(data)    
        subjTC.on_next(data)        


    def processHTag(data):
        subjHI = Subject()
        subjHEmpty = Subject()
        subjHPEI = Subject()

        def processHI(data):
            line_num, tag, line, transaction_log = data        
            action, elements = actionWord(line_num, 2, wlines)

            if action == "<H><I>":
                pline_num, ptag, pline = elements[1]

                subjMerge.on_next((pline_num, ptag, pline, line_num, tag, line, transaction_log))
                subjRemove.on_next((line_num, tag, line))

        def processHEmpty(data):
            line_num, tag, line, transaction_log = data        
            action, elements = actionWord(line_num, 2, wlines)

            if action == "<H><>":
                pline_num, ptag, pline = elements[1]

                subjMerge.on_next((pline_num, ptag, pline, line_num, tag, line, transaction_log))
                subjRemove.on_next((line_num, tag, line))

    # [428]<I>6 SUBMARINE TECHNICAL SUPPORT CENTER (SUBTECHSUPCEN)
    # [429]<>ECHELON CHAIN OF COMMAND
    # [430]<P>11
    # [431]<H>(0034A)    
        def processHPEI(data):
            line_num, tag, line, transaction_log = data        
            action, elements = actionWord(line_num, 4, wlines)

            if action == "<H><P><><I>":
                pline_num, ptag, pline = elements[1]
                eline_num, etag, eline = elements[2]
                iline_num, itag, iline = elements[3]

#                 print(line_num, tag, line, iline_num, itag, iline)
                subjMerge.on_next((iline_num, itag, iline, line_num, tag, line, transaction_log))
                subjRemove.on_next((eline_num, etag, eline))
                subjRemove.on_next((pline_num, ptag, pline))
                subjRemove.on_next((line_num, tag, line))

        subjHI.subscribe(
           lambda x: processHI(x)        
        )     

        subjHEmpty.subscribe(
           lambda x: processHEmpty(x)        
        )

        subjHPEI.subscribe(
           lambda x: processHPEI(x)        
        )

        # this is the start of this function

        subjHI.on_next(data)
        subjHEmpty.on_next(data)
        subjHPEI.on_next(data)    


    def processPTag(data):

        def processPE(data):

            # [395][429]<>ECHELON CHAIN OF COMMAND
            # [396][430]<P>11
            line_num, tag, line, transaction_log = data        
            action, elements = actionWord(line_num, 2, wlines)


            if action == "<P><>":
                eline_num, etag, eline = elements[1]

                rgx1 = re.search('^\s*ECHELON CHAIN OF COMMAND\s*$', eline)
                rgx2 = re.search('^\s*\d+\d*\s*$',line)

                if rgx1 and rgx2:

                    subjRemove.on_next((line_num, tag, line))
                    subjRemove.on_next((eline_num, etag, eline))


        subjPE = Subject()
        subjPE.subscribe(
            lambda x: processPE(x)        
        ) 

        subjPE.on_next(data)    

    # do the H
    source.subscribe(
       on_next = lambda d: processHTag(d),
       on_error = lambda e: print("Error : {0}".format(e)),
       on_completed = lambda: print("Finished processing H tags"),
    )

    # do the T
    source.subscribe(
       on_next = lambda d: processTTag(d),
       on_error = lambda e: print("Error : {0}".format(e)),
       on_completed = lambda: print("Finished processing T tags"),
    )

    # do the P
    source.subscribe(
       on_next = lambda d: processPTag(d),
       on_error = lambda e: print("Error : {0}".format(e)),
       on_completed = lambda: print("Finished processing P tags"),
    )    
    
    return transaction_log


def applyTransactionLog(wlines, transaction_log):
    def mapBreak(line):
        regex = '(^\[\d+\])\s+(<\w*>)\s+(.*$)'
        rgx = re.match(regex, line.strip())
        if rgx:
            line = (rgx.groups())    

        return line    

    def executePlan(data):
        line_num, tag, line = data
        num = getLineNum(line_num)
        if(tag == "<M>"):
            wdict[line_num] = (tag, line)
        elif(tag == "<R>"):
            if line_num in wdict:
                wdict.pop(line_num)   
                
    wdict = { idx : (tag, line) for idx,tag,line in wlines }                

    rxsource3 = rx.from_(transaction_log).pipe(
                    ops.map(lambda x: mapBreak(x))
                )

    rxsource3.subscribe(
       on_next = lambda i: executePlan(i),
       on_error = lambda e: print("Error : {0}".format(e)),
       on_completed = lambda: print('Processed WDictionary: %s' % len(wdict.items())),
    )
    
    rxsource5 = rx.from_(wdict.items()).pipe(
        ops.map(lambda x: x[1][1])
    )

    tmp=[]
    rxsource5.subscribe(
       on_next = lambda i: tmp.append(i),
       on_error = lambda e: print("Error : {0}".format(e)),
       on_completed = lambda: print('Processed: %s' % len(tmp) ),
    ) 
    
    return tmp



def getVerticalBarSepValues(lines):
    template = '^(\d)\s+(.*)\(([\dA-Z]{5})\)$'
    
    rejects = []

    def pickOut(x):
        result = False

        s = re.search(template, x)    
        if s:
            result = True
        else:
            rejects.append(x)
            
        return s

    def breakMap(x):
        result = set()
        rgx = re.match(template, x) 
        if rgx:
            result = rgx.groups()
        return result

    def barSepValues(x):
        echelon, name, uic = x
        return "%s|%s|%s" % (echelon, name.strip(), uic)

    rxsp = rx.from_(lines[:]).pipe(
        ops.filter(lambda x: pickOut(x)),
        ops.map(lambda x: breakMap(x)),
        ops.map(lambda x: barSepValues(x))
    )

    outputs = []

    rxsp.subscribe(
       on_next = lambda i: outputs.append(i),
       on_error = lambda e: print("Error : {0}".format(e)),
       on_completed = lambda: print('Processed: %s' % len(outputs)),
    )
    return outputs, rejects


